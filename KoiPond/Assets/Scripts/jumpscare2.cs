using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpscare2 : MonoBehaviour
{
    public GameObject player;
    public AudioSource scare;
    public GameObject face;
    public GameObject collider;
    public float timeRemaining = 0.5f;

    bool m_isSpook;
    bool m_timer;
    // Start is called before the first frame update
    void Start()
    {
        m_isSpook = true;
        m_timer = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_timer)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                face.SetActive(false);
                timeRemaining = 0;
                m_timer = false;
                collider.SetActive(false);
            }
        }
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player && m_isSpook)
        {
            scare.Play();
            face.SetActive(true);
            m_timer = true;
        }
    }
}