using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpscare : MonoBehaviour
{
    public GameObject player;
    public Animator animator;
    public AudioSource scare;

    bool m_isPlayerScared;
    // Start is called before the first frame update
    void Start()
    {
        m_isPlayerScared = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player && m_isPlayerScared)
        {
            animator.Play("jumpscare");
            scare.Play();
            m_isPlayerScared = false;
        }
    }
}
